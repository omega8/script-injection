(function (){
    var triggerInput = function (dom){
		var evt = document.createEvent("Event");
        evt.initEvent("input", false, true);
        dom.dispatchEvent(evt);
    };
    var startLogin = function (account, password){
		var userDom = document.querySelector(".username");
		var passDom = document.querySelector(".password");
        userDom.value = account;
        passDom.value = password;
		triggerInput(userDom);
		triggerInput(passDom);
		setTimeout(function (){
			document.querySelector(".login-button").click();
		});
    };
    var users = [{
        name: "张三",
        account: "zhangsan",
        password: "123456"
    }];
    var ul = document.createElement("ul");
    ul.setAttribute("style","list-style-type: none;margin: 0;background-color: #CCC;position: fixed;z-index: 9999;top: 0;bottom: 0;right: 0;padding: 0.25em;font-size: 16px;");
    users.forEach(function (user){
        var li = document.createElement("li");
        li.setAttribute("style","padding-top: 0.25em;padding-bottom: 0.25em;");
        var a = document.createElement("a");
        a.innerText = user.name;
        a.setAttribute("href","javascript:void(0);");
        a.addEventListener("click", function (){
            startLogin(user.account, user.password);
        });
        li.appendChild(a);
        ul.appendChild(li);
    });
    document.body.appendChild(ul);
})();