if(mappings && 0 < mappings.length){
	mappings.forEach(function (mapping){
		var matched = mapping.patternList.some(function (pattern){
			if(pattern instanceof RegExp){
				return pattern.test(window.location.href);
			}
			if(pattern instanceof String){
				return -1 != window.location.href.indexOf(pattern);
			}
			return false;
		});
		if(matched){
			mapping.resList.forEach(function (url){
				if(/^.*\.html$/i.test(url)){
					var linkDom = document.createElement("link");
					linkDom.setAttribute("rel","import");
					linkDom.setAttribute("href",(-1 != url.indexOf("?")?(url+"&v="+Math.random()):(url+"?v="+Math.random())));
					document.body.appendChild(linkDom);
				} else if(/^.*\.js$/i.test(url)){
					var script = document.createElement("script");
					script.setAttribute("type","text/javascript");
					script.setAttribute("src",(-1 != url.indexOf("?")?(url+"&v="+Math.random()):(url+"?v="+Math.random())));
					document.body.appendChild(script);
				} else if(/^.*\.css$/i.test(url)){
					var linkDom = document.createElement("script");
					linkDom.setAttribute("rel","stylesheet");
					linkDom.setAttribute("type","text/css");
					linkDom.setAttribute("href",(-1 != url.indexOf("?")?(url+"&v="+Math.random()):(url+"?v="+Math.random())));
					document.body.appendChild(linkDom);
				}
			});
		}
	});
}